class Hamburger {
  constructor(size, stuffing) {
    try {
      if (!arguments.length)
        throw new HamburgerExeption("No size given")

      if (arguments.length === 1)
        throw new HamburgerExeption("No stuffing given")

      if (size.type !== "size")
        throw new HamburgerExeption(`Invalid size ${size.name}`)

      if (stuffing.type !== "stuffing")
        throw new HamburgerExeption(`Invalid stuffing ${stuffing.name}`)

      this._size = size;
      this._stuffing = stuffing;
      this._toppings = [];
    } catch (e) {
      console.log(`${e.name}: ${e.message}`)
    }
  }

  set topping(topping) {
    try {
      if (topping.type !== "topping")
        throw new HamburgerExeption(`Invalid topping ${topping.name}`)

      if (!this._toppings.includes(topping)) {
        this._toppings.push(topping);
      } else {
        throw new HamburgerExeption(`Duplicate topping ${topping.name}`)
      }
    } catch (e) {
      console.log(`${e.name}: ${e.message}`)
    }
  }

  removeTopping(topping) {
    try {
      if (topping.type !== "topping")
        throw new HamburgerExeption(`Invalid topping ${topping.name}`)

      let toppings = this._toppings;
      if (toppings.includes(topping)) {
        toppings.splice(toppings.indexOf(topping), 1);
      } else {
        throw new HamburgerExeption(`Hamburger doesn't have ${topping.name}`)
      }
    } catch (e) {
      console.log(`${e.name}: ${e.message}`)
    }
  }

  get toppings() {
    return console.table(this._toppings); 
  }

  get size() {
    return console.log(this._size)
  }

  get stuffing() {
    return this._stuffing;
  }

  calculatePrice() {
    let toppings = this._toppings;
    let toppingsPricesSum = toppings.reduce(function (total, item) {
      return total + item.price
    }, 0);
    // let toppingsPricesSum = this.toppings.reduce((total, item) => total + item.price, 0);
    let price = this._size.price + this._stuffing.price + toppingsPricesSum;
    console.log("Общая стоимость составляет: " + price);
  }

  calculateCalories() {
    let calories = this._size.cal + this._stuffing.cal;
    for (let i = 0; i < this._toppings.length; i++) {
      calories += this._toppings[i].cal;
    }
    return console.log("Количество калорий: " + calories);
  }

}
// _____________hamburger-type_________

Hamburger.SIZE_SMALL = {
  type: "size",
  name: "SIZE_SMALL",
  price: 50,
  cal: 20
}
Hamburger.SIZE_LARGE = {
  type: "size",
  name: "SIZE_LARGE",
  price: 100,
  cal: 40
}

// _______________stuffing________________


Hamburger.STUFFING_CHEESE = {
  type: "stuffing",
  name: "STUFFING_CHEESE",
  price: 10,
  cal: 20
}
Hamburger.STUFFING_SALAD = {
  type: "stuffing",
  name: "STUFFING_SALAD",
  price: 20,
  cal: 5
}
Hamburger.STUFFING_POTATO = {
  type: "stuffing",
  name: "STUFFING_POTATO",
  price: 15,
  cal: 10
}

// __________________topping__________________


Hamburger.TOPPING_MAYO = {
  type: "topping",
  name: "TOPPING_MAYO",
  price: 20,
  cal: 5
}
Hamburger.TOPPING_SPICE = {
  type: "topping",
  name: "TOPPING_SPICE",
  price: 15,
  cal: 0
}
Hamburger.TOPPING_MUSTARD = {
  type: "topping",
  name: "TOPPING_MUSTARD",
  price: 10,
  cal: 5
}

Hamburger.TOPPING_SAUCE = {
  type: "topping",
  name: "TOPPING_SAUCE",
  price: 10,
  cal: 5
}

class HamburgerExeption {
  constructor(message) {
    this.message = message
    this.name = "HamburgerExeption"
  }
}


let hamburgerOne = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO)

hamburgerOne.topping = Hamburger.TOPPING_SPICE
hamburgerOne.topping = Hamburger.TOPPING_MAYO
hamburgerOne.topping = Hamburger.TOPPING_MUSTARD
hamburgerOne.topping = Hamburger.TOPPING_SAUCE
hamburgerOne.toppings
hamburgerOne.removeTopping(Hamburger.TOPPING_SPICE)
hamburgerOne.removeTopping(Hamburger.TOPPING_MUSTARD)
hamburgerOne.toppings
hamburgerOne.size
hamburgerOne.topping = Hamburger.TOPPING_MUSTARD
hamburgerOne.calculatePrice()
hamburgerOne.calculateCalories()


