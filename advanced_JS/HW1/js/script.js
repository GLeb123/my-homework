function Hamburger(size, stuffing) {
  try {
    if (!arguments.length)
      throw new HamburgerException("No size given")

    if (arguments.length === 1)
      throw new HamburgerException("No stuffing given")

    if (size.type !== "size")
      throw new HamburgerExeption(`Invalid size ${size.name}`)

    if (stuffing.type !== "stuffing")
      throw new HamburgerExeption(`Invalid stuffing ${stuffing.name}`)

    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];

  } catch (e) {
    console.log(`${e.name}: ${e.message}`);
  }
}

// _____________hamburger-type_________

Hamburger.SIZE_SMALL = {
  type: "size",
  name: "SIZE_SMALL",
  price: 50,
  cal: 20
}

Hamburger.SIZE_LARGE = {
  type: "size",
  name: "SIZE_LARGE",
  price: 90,
  cal: 40
}

// _______________stuffing________________

Hamburger.STUFFING_CHEESE = {
  type: "stuffing",
  name: "STUFFING_CHEESE",
  price: 10,
  cal: 20
}

Hamburger.STUFFING_SALAD = {
  type: "stuffing",
  name: "STUFFING_CHEESE",
  price: 20,
  cal: 5
}

Hamburger.STUFFING_POTATO = {
  type: "stuffing",
  name: "STUFFING_POTATO",
  price: 15,
  cal: 10
}

// __________________topping__________________

Hamburger.TOPPING_MAYO = {
  type: "topping",
  name: "TOPPING_MAYO",
  price: 20,
  cal: 5
}

Hamburger.TOPPING_SPICE = {
  type: "topping",
  name: "TOPPING_SPICE",
  price: 15,
  cal: 0
}

Hamburger.TOPPING_MUSTARD = {
  type: "topping",
  name: "TOPPING_MUSTARD",
  price: 10,
  cal: 5
}

Hamburger.TOPPING_SAUCE = {
  type: "topping",
  name: "TOPPING_SAUCE",
  price: 10,
  cal: 5
}


Hamburger.prototype.addTopping = function (topping) {
	try{
    if (topping.type !== "topping") {
      throw new HamburgerExeption(`Invalid topping ${topping.name}`)
  } else {
    this.toppings.push(topping);
  }
			
		if (!this.toppings.includes(topping)){
			this.toppings.push(topping);
		} else {
			throw new HamburgerExeption(`Duplicate topping ${topping.name}`)
		}
	} catch(e){
		console.log(`${e.name}: ${e.message}`)
	}
}

Hamburger.prototype.getToppings = function () {
  return console.table(this.toppings);
}


Hamburger.prototype.removeTopping = function(topping){
	try{
		if(topping.type !== "topping")
			throw new HamburgerExeption(`Invalid topping ${topping.name}`)

		let toppings = this.toppings;
		if(toppings.includes(topping)){
			toppings.splice(toppings.indexOf(topping), 1);
		}else{
			throw new HamburgerExeption(`Hamburger doesn't have ${topping.name}`)
		}
	}catch(e){
		console.log(`${e.name}: ${e.message}`)
	}
}

Hamburger.prototype.getSize = function () {
  return console.log(this.size);
}

Hamburger.prototype.getStuffing = function () {
  return console.log(this.stuffing);
}

Hamburger.prototype.calculatePrice = function () {
  // let toppings = this.toppings;
  // let toppingsPricesSum = this.toppings.reduce(function (total, item) {
  //   return total + item.price
  // }, 0);
  let toppingsPricesSum = this.toppings.reduce((total, item) =>  total + item.price, 0);
  let price = this.size.price + this.stuffing.price + toppingsPricesSum;
  console.log("Общая стоимость составляет: " + price);
}

Hamburger.prototype.calculateCalories = function () {
  let calories = this.size.cal + this.stuffing.cal;
  for (let i = 0; i < this.toppings.length; i++){
    calories += this.toppings[i].cal;
  }
  return console.log("Количество калорий: " + calories);
}

function HamburgerExeption(message) {
  this.message = message
  this.name = "HamburgerExeption"
}

let hamburgerOne = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE)


hamburgerOne.getSize();
hamburgerOne.addTopping(Hamburger.TOPPING_MAYO);
hamburgerOne.getToppings()
hamburgerOne.addTopping(Hamburger.TOPPING_SPICE);
hamburgerOne.getToppings()
hamburgerOne.addTopping(Hamburger.TOPPING_SAUCE);
hamburgerOne.getToppings()
hamburgerOne.removeTopping(Hamburger.TOPPING_MAYO)
hamburgerOne.getToppings()

hamburgerOne.getSize()
hamburgerOne.getStuffing()
hamburgerOne.calculatePrice()
hamburgerOne.calculateCalories()



