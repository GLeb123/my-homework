// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.

// Технические требования:

// В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

const tabs = document.getElementById('tabs');
const tabContent = document.getElementById('tabContent');

for (let i = 0; i < tabContent.children.length; i++){
	tabs.children[i].dataset.index = i;
	if(i)
		tabContent.children[i].hidden = true;
}

tabs.onclick = e => {
	tabs.querySelector(".active").classList.toggle("active");
	tabContent.querySelector("li:not([hidden])").hidden = true;
	e.target.classList.toggle("active");
	tabContent.children[e.target.dataset.index].hidden = false;
}
