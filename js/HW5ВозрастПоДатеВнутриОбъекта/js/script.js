// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.

// Технические требования:

// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


function createNewUser() {
  let userName = prompt('Enter your name');
  while (userName === null || userName === '') {
    userName = prompt('Enter your name, again');
  }
  let userSurname = prompt('Enter your surname');
  while (userSurname === null || userSurname === '') {
    userSurname = prompt('Enter your surname, again');
  }
  let userBirthday = prompt('Enter your birthday', 'dd.mm.yyyy');
  let newUser = {
    firstName: userName,
    lastName: userSurname,
    birthday: userBirthday,
    getLogin() {
      let fullName = (this.firstName.charAt(0) + this.lastName).toLowerCase();
      return fullName;
    },
    getAge() {
      let parseDate = this.birthday.split('.');
      let userDate = new Date(parseDate[2], parseDate[1] - 1, parseDate[0]);
      let userYears = Math.floor((new Date() - +userDate) / 3.154e10);
      return userYears;
    },
    getPassword() {
      let userPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
      return userPassword;
    }
  };
  console.log(newUser.getLogin());
  console.log(newUser.getAge());
  console.log(newUser.getPassword());
  return newUser;
}
console.log(createNewUser());