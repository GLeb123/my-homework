// Задание
// Реализовать функцию для создания объекта "пользователь".

// Технические требования:

// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.

//Задание продвинутой сложности

// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

// ____________________________

// Метод объекта - это функция с набором инструментом внутри для работы с данным объектом

// ____________________________

function createNewUser() {
  let userName = prompt('Enter your name');
  while (userName === null || userName === '') {
    userName = prompt('Enter your name, again');
  }
  let userSurname = prompt('Enter your surname');
  while (userSurname === null || userSurname === '') {
    userSurname = prompt('Enter your surname, again');
  }
  let userBirthday = prompt('Enter your birthday', 'dd.mm.yyyy');
  let newUser = {
    firstName: userName,
    lastName: userSurname,
    birthday: userBirthday,
    getLogin() {
      let fullName = (this.firstName.charAt(0) + this.lastName).toLowerCase();
      return fullName;
    },
    getAge() {
      let parseDate = this.birthday.split('.');
      let userDate = new Date(parseDate[2], parseDate[1] - 1, parseDate[0]);
      let userYears = Math.floor((new Date() - +userDate) / 3.154e10);
      return userYears;
    },
    getPassword() {
      let userPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
      return userPassword;
    }
  };
  console.log(newUser.getLogin());
  console.log(newUser.getAge());
  console.log(newUser.getPassword());
  return newUser;
}
console.log(createNewUser());