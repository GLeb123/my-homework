// Задание
// Реализовать функцию для создания объекта "пользователь".

// Технические требования:

// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.

//Задание продвинутой сложности

// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

// ____________________________

// Метод объекта - это функция с набором инструментом внутри для работы с данным объектом

// ____________________________

function createNewUser() {
  let userName = prompt('Enter your name', 'Givi');
  while (userName === null || userName === '') {
    userName = prompt('Enter your name,again');
  }
  let userSurname = prompt('Enter your surname', 'Doe');
  while (userSurname === null || userSurname === '') {
    userSurname = prompt('Enter your surname, again');
  }
  let userBirthday = prompt('Enter your birthday', '04.06.1991');
  // Тут всё ясно.Для начала забрали имя,фамилию и дату рождения в переменную с помощью промпт.
  // Теперь создаём объект
  let newUser = {
    firstName: userName,
    lastName: userSurname,
    birsday: userBirthday,
    // Внутри объекта создаём метод - функцию
    getLogin() {
      let fullName = (this.firstName.charAt(0) + this.lastName).toLowerCase();
      return fullName;
      // Метод сharAt(0).Возвращает символ строки по указанному индексу
      // this в данном случае указывает на сам объект,т.е запись могла быть следующая :newUser.firstName.charAt(0)
    },
    getAge() {
      let parseDate = this.birthday.split('.');
      // Задали переменную parseDate.Внутри обратились к свойству birthday
      // и попросили разбить через точку
    }
  }
  console.log(newUser);
  console.log(newUser.getLogin());
}
createNewUser()

// Пример как вырезать и сложить отдельные символы
// getLogin внутри объекта является функцией,внутри которой удобно работать
// с остальными свойствами объекта.
// внутрь метода кладём переменную,в которую пакуем опреацию,и затем просим наш метод вернуть значение этой переменной

// let firstUserName = 'Givi';
// let seconUserdName = 'Vanya';
// let thirdUserName = 'Vasya';

// let arr = {
//   firstName: firstUserName,
//   secondName: seconUserdName,
//   thirdName: thirdUserName,
//   getLogin(){
//     let fullName = (arr.firstName.charAt(0) + this.secondName.charAt(2) + this.secondName.charAt(1));
//     return fullName;
//   }
// }
// console.log(arr.getLogin());

// Можно сделать это и не внутри метода тогда не пользуемся this

// let firstUserName = 'Givi';
// let seconUserdName = 'Vanya';
// let thirdUserName = 'Vasya';

// let arr = {
//   firstName: firstUserName,
//   secondName: seconUserdName,
//   thirdName: thirdUserName,
// }
// function getLogin() {
//   let fullName = (arr.firstName.charAt(0) + arr.secondName.charAt(2) + arr.secondName.charAt(1));
//   return fullName;
// }
// console.log(getLogin());

// 1.задали переменные
// 2.создали объект и в свойства сложили значения переменных
// 3.Создали функцию,внтрь положили переменную и в переменную результат вычислений
// 4.Вернули функцией переменную